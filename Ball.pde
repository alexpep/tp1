class Ball extends GraphicObject{
  
  float masse;
  float rayon=30;
 
  
  
  Ball(PVector location, PVector vel){
    this.location = location;
    this.velocity = vel;
    this.acceleration = new PVector (0 , 0);
    this.masse=3;
    
}

 void applyForce(PVector force) {
    PVector f;
    
    f = PVector.div (force, masse);
    this.acceleration.add(f);   
  }
  

  void checkEdges() {
    if (location.x > width) {
        location.x = width;
        velocity.mult(0.9);
        velocity.x *= -1;
    } 
    else if (location.x < 0) {
        velocity.mult(0.9);
        velocity.x *= -1;
        location.x = 0;
    }
 
    else if (location.y+rayon/2> height) {
      velocity.mult(0.9);
      velocity.y *= -1;
      location.y = height-rayon/2;
    }
    
    else if(location.y-rayon<0){
     velocity.mult(0.9);
    velocity.y *= -1;
    }
  }
  


void update(float deltaTime){

     velocity.add(acceleration);
     location.add(velocity);
     acceleration.mult(0); 
     
  }


void display(){ 
   fill(255);
   ellipse(location.x,location.y,rayon,rayon);
  }
  
}
