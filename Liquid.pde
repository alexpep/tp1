class Liquid extends GraphicObject{
PShape liquid;
float density;
float hauteurmulti=0.2;
boolean show=true;


Liquid(PVector loca){
  this.location=loca;
  this.velocity=new PVector(0,0);
  this.acceleration=new PVector(0,0);
  density=random(1.5,3);
  this.liquid=createShape(RECT,this.location.x, this.location.y, width*0.25,height*hauteurmulti);
}

void update(float deltaTime){

}

void display(){
    liquid.setFill(color(30,144,255));
    shape(liquid);
   fill(0);
 text(density,width*0.875,height*(1-hauteurmulti)+(height*hauteurmulti)/2);
 text("Alexandre Pépin",width*0.875,height*(1-hauteurmulti)+(height*hauteurmulti)/1.75);
 
}











}
