class Mover extends GraphicObject {
float masse;
boolean impact=false;
boolean edge;
float rayon;
boolean gothit;



  Mover () {
    location = new PVector();
    velocity = new PVector();
    acceleration = new PVector();
  }
  
  Mover (PVector loc, PVector vel) {
    this.location = loc;
    this.velocity = vel;
    this.acceleration = new PVector (0 , 0);
    this.masse=random(10,16);
    gothit=false;
    rayon=45;
    
  }
  
  void update(float deltaTime) {
    if(!edge){
    velocity.add(acceleration);
    location.add(velocity);
    acceleration.mult(0); 
    }

  }
  
  void display() {
    fill(89); 
    ellipse(location.x,location.y,rayon,rayon);
  }
  
void checkEdges() {
    if (location.y+rayon/2 > height) {
      velocity.y *= -1;
      location.y = height-rayon/2;
      edge=true;
    }
}
  
  
 
 void applyForce(PVector force) {
    PVector f;
    f = PVector.div (force, masse);
    this.acceleration.add(f);   
}

boolean isInsideliquid(Liquid l) {
   if(l.location.x  <= this.location.x&& l.location.y <= this.location.y+rayon/2)
        {
          return true; 
        }
   else  return false;
  
}

void objectcontact(Ball obj){
  if( PVector.dist(obj.location,location) <=obj.rayon+this.rayon)
  {
    gothit=true;
  
  }
}

 void drag(Liquid l) {
 
    float speed = velocity.mag();
    float dragMagnitude = l.density * speed * speed;
 
    PVector drag = this.velocity.copy();
    drag.mult(-1);
 
    drag.setMag(dragMagnitude);

    applyForce(drag);
  }
}
