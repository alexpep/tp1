int currentTime;
int previousTime;
int deltaTime;
PVector  loca; 
ArrayList<Mover> targets ;
ArrayList<Ball> balls ;
int targetnb;
float angle;
float forcewind;
Canon c ;
Liquid liquid;


void setup () {
  fullScreen(P2D);
  reset();
 
  }

void draw () {
  //Force set
  
 PVector gravity = new PVector(0,0.1);
 PVector wind = new PVector(forcewind,0);

  

   for(int i = 0; i < balls.size(); i++){
    

    balls.get(i).applyForce(gravity);
    balls.get(i).applyForce(wind);
    balls.get(i).update(0);
    balls.get(i).checkEdges();
   }
  
  //gravity+wind

   for(int j=0;j<balls.size();j++)
   {
      for(int i=0;i<targets.size();i++)
       {
         targets.get(i).objectcontact(balls.get(j));
           if(targets.get(i).gothit)
           {
              if(targets.get(i).isInsideliquid(liquid) && liquid.show)
              {
                targets.get(i).drag(liquid); 
              }
              
               targets.get(i).applyForce(gravity);
               targets.get(i).applyForce(wind);
               targets.get(i).update(0);
               targets.get(i).checkEdges();
          } 
       } 
 }

  display();
}

void display () {
 background(0);
 
 if(liquid.show)
 liquid.display();
 
 for (Mover t : targets) {
    t.display();
  }
  

 c.display(); 
 
 for(Ball b : balls){
    b.display(); 
 }
 
}


//move canon with a/d and reset with r
void keyPressed(){
  if((key=='A' || key == 'a')&& c.angletot>-90)
  {  
    angle=-2;
    c.activeRotation(angle);
  }
    if((key=='D' || key == 'd' ) && c.angletot<0)
  {  
    angle=2;
    c.activeRotation(angle);  
  }

  else if(key=='R' || key == 'r')
  {
     reset();   
  }
  else if(key=='V' || key == 'v')
  {
    if(liquid.show)
    liquid.show=false;
    else  liquid.show=true;       
  }
  
  if(key==' ')
  {
 
    Ball b = new Ball(new PVector(width*0.05,c.location.y-(c.hauteur*0.8)),new PVector(18*cos(radians(c.getAngle())),19*sin(radians(c.getAngle()))) );
    balls.add(b);
  }

}

void mousePressed() {
  if (mouseButton == LEFT) {
    forcewind=-0.2;
  } 
  else {
    forcewind=0.2;
  }
}
void mouseReleased()
{

  if (mouseButton == LEFT) {
    for( int i=0;i<balls.size();i++){
    
    forcewind=0;
    
    }
  } 
  else {
    forcewind=0;
  }

}


//reset game
void reset()
{
 int cpt=0;
  targetnb=20;
  angle=0.0;
  forcewind=0;
  //canon set
  loca=new PVector(0.0,height,0.0);
  c=new Canon(new PVector(0,height),angle);

    
  //targets(20) set
  loca.x=random(0.4,1)*width;
  loca.y=random(0.1,0.8)*height;
  balls= new ArrayList<Ball>();
  targets = new ArrayList<Mover>();
  for (int i = 0; i < 7; i++) {
    for(int j=0;j<3 ;j++)
    {
      if(cpt<20)
      {
      Mover t = new Mover( new PVector(width*0.75+j*(width*0.07)+j*(width*0.02),height*0.05+(i*0.10*height)), new PVector(0,0));
      targets.add(t); 
      cpt++;
      }
    }
  
    
  //liquid set
  liquid=new Liquid(new PVector(width*0.75,height*0.8));
  }
  
 

}
